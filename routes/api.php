<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!

|//https://www.youtube.com/watch?v=jIzPuM76-nI&list=LL&index=2
    /// min 4:08
*/
Route::get('/user',[\App\Http\Controllers\AuthentificationController::class,'user']); 